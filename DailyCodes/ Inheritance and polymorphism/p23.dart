abstract class Developer{

  int x = 10;

  Developer(){
    print("In developer constructor");
  }

  void develop(){
    print("We develop software");
  }

  void devType();
}

class Mobiledev implements Developer{

int x =20;

  Mobiledev(){
    print("In MobileDev Constructor ");
  }

  void develop(){
    print("We develope mobile software");
  }

  void devType(){
    print("FLutter developer");
  }
}