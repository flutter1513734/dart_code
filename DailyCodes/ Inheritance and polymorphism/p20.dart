/* Abstract class

Output:
gold,diamond,real estate,flat
Engineering
Momoo */

abstract class Parent{

  void property(){
    print("gold,diamond,real estate,flat");
  }

void career();

void marry();

}
abstract class Child extends Parent{
    void career(){
      print("Engineering");
    }
}
class child1 extends Child {
  void marry(){
    print("Momoo");
  }
}
void main(){
  Parent obj = new child1();
  obj.property();
  obj.career();
  obj.marry();
}
