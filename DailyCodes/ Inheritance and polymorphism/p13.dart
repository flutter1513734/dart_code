/*Type 3 : Hierarchical Inheritance 
o/p:
In Para Cons
In Para Cons
In Child1 Cons
In Para Cons
In Child2 Cons */

class Parent{
  Parent(){
    print("In Para Cons");
  }
}
class Child1 extends Parent{
  Child1(){
    print("In Child1 Cons");
  }
}
class Child2 extends Parent{
  Child2(){
    print("In Child2 Cons");
  }
}
void main(){
  Parent obj1 = new Parent();
  Child1 obj2 = new Child1();
  Child2 obj3 = new Child2();

}