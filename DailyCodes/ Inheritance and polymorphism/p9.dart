/*o/p:
In para cons
In call
In child1 cons */

class Parent{

  Parent(){
    print("In para cons");
  }
  call(){
    print("In call");
  }
}

class Child1 extends Parent{
  Child1(){
    super();
    print("In child1 cons");
  }
}
void main(){
  Child1 obj = new Child1();
}