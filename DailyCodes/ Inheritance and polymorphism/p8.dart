/* Error:
Error: Superclass has no method named 'call'.
    super();
    ^^^^ */
class Parent{

  Parent(){
    print("In para cons");
  }
}

class Child1 extends Parent{
  Child1(){
    super();
    print("In child1 cons");
  }
}
void main(){
  Child1 obj = new Child1();
  
}