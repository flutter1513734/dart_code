/*Passing argument to child constructor
Error:
The superclass, 'Parent', has no unnamed constructor that takes no arguments.
  Child(this.y,this.name);
  ^^^^^
 */
class Parent{
  int? x;
  String? str;

  Parent(this.x,this.str);

  void PrintData(){
    print(x);
    print(str);
  }
}
class Child extends Parent{
  int? y;
  String? name;

  Child(this.y,this.name){

  }
  void disData(){
   print(y);
   print(name);
  }
}
void main(){
  Child obj = new Child(10, "teja");

}