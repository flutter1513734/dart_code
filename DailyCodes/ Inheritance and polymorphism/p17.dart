/* passing argument to child constructor 
1
teja
Google
Silicon valley */

class Company{

  String? cmpName;
  String? cmpLoc;

  Company(this.cmpName,this.cmpLoc);

  void compInfo(){
    print(cmpName);
    print(cmpLoc);
  }
}
class Employee extends Company{

  int? empId;
  String? empName;

  Employee(this.empId,this.empName,String cmpName,String cmpLoc) : super(cmpName,cmpLoc);

  void empInfo(){
    print(empId);
    print(empName);
  }
}
void main(){

  Employee obj = new Employee(1,"teja","Google","Silicon valley");
 
  obj.empInfo();
   obj.compInfo();
}