 /* o/p:                Note: if the variable name of parent and child is declaredas same then only
                           the constructor of parent class will not comes in the child class 
In para Cons
895454723
In child cons
895454723
20
20*/
 class Parent{

int x =10;

  Parent(){
    print("In para Cons");
    print(this.hashCode);
  }

  void m1(){
    print(x);
  }

 }
 class Child extends Parent {

int x = 20;

  Child(){
    print("In child cons");
    print(this.hashCode);
  }

  void m2(){
    print(x);
  }
 }

 void main(){

  Child obj = new Child();
  obj.m2();
  obj.m1();
 }