/* Polymorphism =>> Overloading --> Dart don't allows the Overloading

Error: 'disp' is already declared in this scope.
  void disp(int x, int y){
       ^^^^
p18.dart:8:8: Context: Previous declaration of 'disp'.
  void disp(int x){
       ^^^^
p18.dart:19:11: Error: Too many positional arguments: 1 allowed, but 2 found.
Try removing the extra positional arguments.
  obj.disp(10, 20);  */

class Demo{

  int? x;
  int? y;

  void disp(int x){
    print(x);
  }

  void disp(int x, int y){
    print(x);
    print(y);
  }
}
void main(){
  Demo obj = new Demo();
  obj.disp(10, 20);
  obj.disp(10);
}