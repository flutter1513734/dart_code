//The child class have access of all methods of the parent class but it cant be access the CONSTRUCTOR of parent class
class Parent{
  int x = 10;
  String name = "tej";

  void ParentMethod(){

    print(x);
    print(name);

  }
}

class Child extends Parent{

  int y =20;
  String name1 = "tejaaa";

  void ChildMethod(){

    print(y);
    print(name1);

  }
}
void main(){

  Child obj = new Child();

  print(obj.x);
  print(obj.name);
  obj.ParentMethod();

print("----------");
  print(obj.y);
  print(obj.name1);
  obj.ChildMethod();
}