/*Real time example :
Output :
We develope software
Flutter developer
We develope software
Web Developer
We develope software
Flutter developer
We develope software
Web Developer  */

abstract class Developer {

  void develop(){
    print("We develope software");
  }

  void devType();
}

class MobileDev extends Developer{

  void devType(){
    print("Flutter developer");
  }
}

class WebDev extends Developer{

  void devType(){
    print("Web Developer");
  }
}

void main(){

  Developer obj1 = new MobileDev();
  obj1.develop();
  obj1.devType();

  Developer obj2 = new WebDev();
  obj2.develop();
  obj2.devType();

  MobileDev obj3 = new MobileDev();
  obj3.develop();
  obj3.devType();

  WebDev obj4 = new WebDev();
  obj4.develop();
  obj4.devType();

}