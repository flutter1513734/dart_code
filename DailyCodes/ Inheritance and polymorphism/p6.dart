
class Parent{

  int x = 10;
  String str = "hii";

  Parent(){
    print("Parent constructor");
  }
}

class Child extends Parent{

  Child(){
    print("Child Constructor");
  }
}

void main(){

  Parent obj1 =new Parent();
  Child obj2 = new Child();

  print(obj2.x);
  print(obj2.str);

  
}