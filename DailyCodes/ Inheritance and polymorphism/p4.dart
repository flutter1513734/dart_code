
class Parent{

  int x = 10;
  String name = "teja";

  get getX => x;
  get getName => name;
}
class Child extends Parent{

  int y = 20;
  String name1 = "Shreya";

  get getY => y;
  get getName1 => name1;
}
void main(){

  Child obj = new Child();

  print(obj.getX);
  print(obj.getName);
  print(obj.y);
  print(obj.name1);
  
}