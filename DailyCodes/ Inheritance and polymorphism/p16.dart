/* passing Argument to child constructor
10
teja
20
Momoo */
class Parent{

  int? x;
  String? str1;

  Parent(this.x,this.str1);

  void parentData(){
    print(x);
    print(str1);
  }
}
class Child extends Parent{

  int? y;
  String? str2;

  Child(this.y,this.str2,int x,String str1) : super(x,str1);

  void childData(){
    print(y);
    print(str2);
  }
}
void main(){

  Child obj = new Child(10, "teja",20,"Momoo");
  obj.childData();
  obj.parentData();
}
