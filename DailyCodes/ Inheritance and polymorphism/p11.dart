/*Types of Inheritance:
1.Multilevel
o/p:
In ICC Cons
In BCCI Cons
In IPL Cons */

class ICC{
  ICC(){
   
      print("In ICC Cons");
  }
}
class BCCI extends ICC{
  BCCI(){
    print("In BCCI Cons");
  }
}

class IPL extends BCCI{
  IPL(){
    print("In IPL Cons");
  }
}

void main(){
  IPL obj = new IPL();
}

