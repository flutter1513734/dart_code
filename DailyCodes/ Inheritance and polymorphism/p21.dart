/* Abstract class--

Output:
In Demo Constructor
In Demo Child Constructor
In Demo fun1
In Demo Child fun2  */

abstract class Demo{

  Demo(){
    print("In Demo Constructor");
  }

  void fun1(){
    print("In Demo fun1");
  }

  void fun2();
}
class DemoChild extends Demo{

  DemoChild(){
    print("In Demo Child Constructor");
  }
  void fun2(){
    print("In Demo Child fun2 ");
  }
}
void main(){
  Demo obj = new DemoChild();
  obj.fun1();
  obj.fun2();
}