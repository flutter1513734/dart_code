/* Overriding in dart : dart allows the overriding in polymorphism

Output:
Amazon Web Services
OTT Platform  */

class Amazon {
  int? emp;
  String? cmpName;

  Amazon(this.emp,this.cmpName);

  void server(){
    print("Amazon Web Services");
  }

  void streaming(){
    print("Streaming");
  }
}
class AmazonPrime extends Amazon{

  int? rating;

  AmazonPrime(this.rating,int emp,String cmpName) : super(emp,cmpName);

  void streaming(){

    print("OTT Platform");
  }
}

void main(){

  Amazon obj = new AmazonPrime(5, 500000, "Amazon");
  obj.server();
  obj.streaming();
}