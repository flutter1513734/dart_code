class Demo{
  int x =10;
  static int y = 20;

  void printData(){
    print(x);
    print(y);
  }
}

void main(){
  Demo obj = new Demo();
  obj.printData();

  print("---");

  Demo.y = 50;
  obj.x = 30;
  
  obj.printData();
}