//Setter metgod - way 2

import 'p11.dart';

void main(){

  Demo obj = new Demo("shreya",33,80.5);

  obj.printData();

  //Explicitely === manually call
  obj.setName("tej");
  obj.setId(10);
  obj.setSal(60.7);

  //Implicitely === Automatic call
  obj.setName= "tejaswini";
  obj.setId = 55;
  obj.setSal = 90.2;

  print("------------");
  obj.printData();
}