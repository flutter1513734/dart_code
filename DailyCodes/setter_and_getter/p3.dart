//Getter method way-1 
class Demo{

  int? _id;
  String? name;
  double? _sal;

  Demo(this.name,this._id,this._sal);

  int? getId(){
    return _id;
  }

  double? getSal(){
    return _sal;
  }

}