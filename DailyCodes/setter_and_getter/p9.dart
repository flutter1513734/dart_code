//Setter method - way 1

class Demo{

  String? name ;
  int? _id;
  double? _sal;

  Demo(this.name,this._id,this._sal);

  String? setName(String name){
    
    this.name = name;

  }

  int? setId(int x){

    _id = x;

  }

  double? setSal(double sal){
    
    _sal = sal;

  }

  void printData(){

    print(_id);
    print(name);
    print(_sal);

  }
}