// setter method - way3
import 'p13.dart';

void main(){

  Demo obj = new Demo(44,"manya",11.11);

  obj.printData();

  //Emplicitely === manually call
  obj.setId(55);
  obj.setName("Momoo");
  obj.setSal(12.12);

 // Implicitely === Automatic
  obj.setId = 55;
  obj.setName = "Momoo";
  obj.setSal = 12.12;

  print("-----------");
  obj.printData();

}