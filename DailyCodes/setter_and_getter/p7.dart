//Getter method - way 3
class Demo{

  String? name;
  int? _id;
  double? _sal;

  Demo(this.name,this._id,this._sal);

  get getId => _id;

  get getSal => _sal;
}