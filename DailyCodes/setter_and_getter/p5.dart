//Getter method - way2
class Demo{

  String? name;
  int? _id;
  double? _sal;

  Demo(this.name,this._id,this._sal);

  int? get getId{
    return _id;
  }

  double? get getSal{
    return _sal;
  }
}