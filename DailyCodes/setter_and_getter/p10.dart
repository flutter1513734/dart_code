//Setter method - way 1

import 'p9.dart';

void main(){

  Demo obj = new Demo("tejaswini",5,50.6);

  obj.printData();

  //Explicietly === manually call
  obj.setName("tej");
  obj.setId(10);
  obj.setSal(60.7);

  print("----------");
  /*Implicitely === automatic call
  obj.setName= "tejaswini";
  obj.setId = 55;
  obj.setSal = 90.2;*/
  obj.printData();

}