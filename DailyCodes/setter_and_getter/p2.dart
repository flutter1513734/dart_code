import 'p1.dart';

void main(){

  Demo obj = new Demo("tej", 1, 10.5);

  print(obj.name);
  print(obj._id);
  print(obj._sal);

}

/* o/p:
p2.dart:8:13: Error: The getter '_id' isn't defined for the class 'Demo'.
 - 'Demo' is from 'p1.dart'.
Try correcting the name to the name of an existing getter, or defining a getter or field named '_id'.
  print(obj._id);
            ^^^
p2.dart:9:13: Error: The getter '_sal' isn't defined for the class 'Demo'.
 - 'Demo' is from 'p1.dart'.
Try correcting the name to the name of an existing getter, or defining a getter or field named '_sal'.
  print(obj._sal);
            ^^^^ */