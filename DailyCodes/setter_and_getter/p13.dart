// setter method - way3

class Demo{

  int? _id;
  String name;
  double _sal;

  Demo(this._id,this.name,this._sal);

  set setId(int id) => _id=id;
  set setName(String name) => this.name=name;
  set setSal(double sal) => _sal =sal;

  void printData(){

    print(_id);
    print(name);
    print(_sal);

  }
}