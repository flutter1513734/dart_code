/*o/p
null
null
*/

class Demo{

  int? x;
  String? str;

  Demo(int x,String str){
    x= x;
    str=str;
  }

  void printData(){
    print(x);
    print(str);
  }
}

void main(){

  Demo obj = new Demo(10,"Teja");

  obj.printData();
}