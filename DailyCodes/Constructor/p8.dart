
class Demo{

  int? x;
  String? str;

  Demo(int x,String str){
    this.x= x;
    this.str=str;
  }

  void printData(){
    print(x);
    print(str);
  }
}

void main(){

  Demo obj = new Demo(10,"Teja");

  print(obj.hashCode);            //property/variable of object class
  print(identityHashCode(obj));   //method of core package
  
}