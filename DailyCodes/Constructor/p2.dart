/*

2.dart:2:3: Error: Constructors can't have a return type.
Try removing the return type.
  void Demo(){
  ^^^^
 */

class Demo{
  void Demo(){
    print("Constructor");
  }
}
void main(){
    new Demo();
}