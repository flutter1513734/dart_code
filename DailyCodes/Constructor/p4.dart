/*o/p:
10
Teja
*/

class Demo{

  int? x ;
  String? str ;

  void printData(){
    print(x);
    print(str);
  }
}

void main(){

  Demo obj = new Demo();

  obj.x = 10;
  obj.str = "Teja";

  obj.printData();
}