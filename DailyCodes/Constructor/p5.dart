/* o/p
10
Teja
*/

class Demo{

  int? x ;
  String? str;

  Demo(int value,String name){
    x=value;
    str=name;
  }

  void printData(){
    print(x);
    print(str);
  }
}

void main(){

  Demo obj = new Demo(10,"Teja");

  obj.printData();
}