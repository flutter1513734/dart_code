/* non constant object
o/p:
730590156
555126093
 */

class Demo{

  int? jerNo;
  String? pName;

  Demo(this.jerNo,this.pName);
}
void main(){

  Demo obj = new Demo(10,"teja");
  print(obj.hashCode);

  Demo obj1 = new Demo(10,"teja");
  print(obj1.hashCode);
}