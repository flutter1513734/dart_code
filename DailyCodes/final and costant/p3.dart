/*constant object - increases the performance of the code
68597650
68597650
*/

class Demo{

   final int? x;
   final String? str;

   const Demo(this.x,this.str);
}

void main(){

  Demo obj1 = const Demo(10,"teja");
  print(obj1.hashCode);
  
  Demo obj2 = const Demo(10, "teja");
  print(obj2.hashCode);

}

