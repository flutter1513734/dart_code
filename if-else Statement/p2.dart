void main(){
  var a = 5;
  var b = 16;

  if(a<10){
    print("$a is less than 10");
  }
  if(b>10){
    print("$b is greater than 10");
  }
}

/*output:
5 is less than 10
16 is greater than 10*/