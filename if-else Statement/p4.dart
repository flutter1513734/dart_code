void main(){
  var a = 5;
  var b = -9;

  if(a>=0){
    print("$a is positive number");
  }

  if(b<0){
    print("$b is a negative number");
  }
}
/* Output:
5 is positive number
-9 is a negative number */