//write a program to print a table og 12 in reverse order
void main(){
  int num = 12;
  for(int i=10;i>=1;i--){
    print(num*i);
  }
}
/*Output
120
108
96
84
72
60
48
36
24
12
*/