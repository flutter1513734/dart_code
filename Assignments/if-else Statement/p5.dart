void main(){
  var a = "A";
  var b = "D";
  if((a == "A") || (a == "E") || (a == "I") || (a == "O") || (a == "U")){
    print("$a  is a Vowel");
  }
  if((b != "A") || (b != "E") || (b != "I") || (b != "O") || (b != "U")){
    print("$b  is a Consonant");
  }
}

/*Output:
A  is a Vowel
D  is a Consonant */