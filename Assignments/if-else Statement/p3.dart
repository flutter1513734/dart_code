void main(){
  int age1 = 18;
  int age2 = 14;
  if(age1>=18){
    print("$age1 : You can cast a vote");
  }
  if(age2 <=18){
    print("$age2 : You can't cast a vote");
  }
}

/* Output
18 : You can cast a vote
14 : You can't cast a vote*/