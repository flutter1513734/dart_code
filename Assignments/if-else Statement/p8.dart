void main(){
  int x = 15;
  int y = 9;
  
  //for x
  if(x%3==0 && x%5==0){
    print("$x : Divisible by both");
  }
  else if(x%3==0){
    print("$x : Divisible by 3");
  }
  else if(x%5==0){
    print("$x : Divisible by 5");
  }
  else{
    print("Not divisible by 3 or 5");
  }
  
  //for y
  if(y%3==0 && y%5==0){
    print("$y : Divisible by both");
  }
  else if(y%3==0){
    print("$y : Divisible by 3");
  }
  else if(y%5==0){
    print("$y : Divisible by 5");
  }
  else{
    print("Not divisible by 3 or 5");
  }
}

/*Output:
15 : Divisible by both
9 : Divisible by 3 */
